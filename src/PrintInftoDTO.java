package com.skilltechgroup.cordova.plugin;

public class PrintInftoDTO {

    private String numeroInfraccion;
    private String dominio;
    private String fecha;
    private String inspector;
    private String idInspector;
    private String printText;
    private String printerMac;

    public String getNumeroInfraccion() {
        return numeroInfraccion;
    }

    public void setNumeroInfraccion(String numeroInfraccion) {
        this.numeroInfraccion = numeroInfraccion;
    }

    public String getDominio() {
        return dominio;
    }

    public void setDominio(String dominio) {
        this.dominio = dominio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getIdInspector() {
        return idInspector;
    }

    public void setIdInspector(String idInspector) {
        this.idInspector = idInspector;
    }

    public String getPrintText() {
        return printText;
    }

    public void setPrintText(String printText) {
        this.printText = printText;
    }

    public String getPrinterMac() {
        return printerMac;
    }

    public void setPrinterMac(String printerMac) {
        this.printerMac = printerMac;
    }
}
