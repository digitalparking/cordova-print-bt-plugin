package com.skilltechgroup.cordova.plugin;
// The native Toast API

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.widget.ImageView;
import android.widget.SeekBar;
import com.google.gson.Gson;
import org.apache.cordova.*;
import org.json.JSONArray;

import java.io.*;
import java.util.*;

// Cordova-required packages

public class BtPrintPlugin extends CordovaPlugin {

    private Context context;

    private static final String TAG = "BXLEXAMPLE";

    public static final int REQUEST_ENABLE_BT = 8807;
    public static final String ACTION_DISCOVER_DEVICES = "listDevices";
    public static final String ACTION_LIST_BOUND_DEVICES = "listBoundDevices";
    public static final String ACTION_IS_BT_ENABLED = "isBTEnabled";
    public static final String ACTION_ENABLE_BT = "enableBT";
    public static final String ACTION_DISABLE_BT = "disableBT";
    public static final String ACTION_PAIR_BT = "pairBT";
    public static final String ACTION_UNPAIR_BT = "unPairBT";
    public static final String ACTION_STOP_DISCOVERING_BT = "stopDiscovering";
    public static final String ACTION_IS_BOUND_BT = "isBound";

    private boolean conn = false;
    private PowerManager pm;
    private PowerManager.WakeLock wl;

    private final static char ESC_CHAR = 0x1B;

    private byte[] INIT_PRINTER = new byte[]{ESC_CHAR, 0x40};

    private final static byte[] LINE_FEED = new byte[]{0x0A};
    //private static byte[] SELECT_BIT_IMAGE_MODE = {0x1D, 0x76, 0x30, 33};
    private static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33};
    private final static byte[] SET_LINE_SPACE_24 = new byte[]{ESC_CHAR, 0x33, 24};
    //private final static byte[] SET_LINE_SPACE_24 = new byte[]{ESC_CHAR, 0x40};
    private final static byte[] PRINT_AND_FEED_PAPER = new byte[]{0x0A};

    private static byte[] SELECT_BNV_IMAGE = {0x1C, 0x70, 33, 3};
    //private byte SELECT_BITMAP_MODE[] = { 0x1B, 0x7C, 0x31, 0x42, 0x00 }; // ESC|SetImage_Print

    String strFor2dBarcode = "";


    private static BluetoothAdapter mBTAdapter;
    private ArrayList<BluetoothDevice> found_devices;
    private boolean discovering = false;
    private BluetoothDevice mBTDevice;
    public static BluetoothSocket mBTSocket;
    public BroadcastReceiver mBTReceiver;


    public static final byte HT = 0x9;
    public static final byte LF = 0x0A;
    public static final byte CR = 0x0D;
    public static final byte ESC = 0x1B;
    public static final byte DLE = 0x10;
    public static final byte GS = 0x1D;
    public static final byte FS = 0x1C;
    public static final byte STX = 0x02;
    public static final byte US = 0x1F;
    public static final byte CAN = 0x18;
    public static final byte CLR = 0x0C;
    public static final byte EOT = 0x04;
    public static final byte NUL = 0x00;
    public static final byte[] LOCATION = {0x1B, 0x44, 0x04, 0x00};
    public static final byte[] ESC_FONT_COLOR_DEFAULT = new byte[]{ESC, 'r', 0x00};
    public static final byte[] FS_FONT_ALIGN = new byte[]{FS, 0x21, 1, ESC,
            0x21, 1};
    public static final byte[] ESC_ALIGN_LEFT = new byte[]{0x1b, 'a', 0x00};

    public static final byte[] ESC_ALIGN_RIGHT = new byte[]{0x1b, 'a', 0x02};

    public static final byte[] ESC_ALIGN_CENTER = new byte[]{0x1b, 'a', 0x01};

    public static final byte[] ESC_CANCEL_BOLD = new byte[]{ESC, 0x45, 0};


    public static final byte[] ESC_BARCODE_FORMAT = new byte[]{(byte) 0x1d, (byte) 0x6b, (byte) 0x49};


    /*********************************************/
    public static final byte[] ESC_HORIZONTAL_CENTERS = new byte[]{ESC, 0x44, 20, 28, 00};

    public static final byte[] ESC_CANCLE_HORIZONTAL_CENTERS = new byte[]{ESC, 0x44, 00};
    /*********************************************/


    public static final byte[] ESC_ENTER = new byte[]{0x1B, 0x4A, 0x40};

    public static final byte[] PRINTE_TEST = new byte[]{0x1D, 0x28, 0x41};

    public static final byte[] NV_LOGO = new byte[]{0x1C, 0x70, 0x01, 0x00};

    public static final byte[] CENTER_POSITION = new byte[]{0x1B, 0x61, 0x01};

    public static final byte[] LEFT_POSITION = new byte[]{0x1B, 0x61, 0x00};

    public static final byte[] H1_SIZE = new byte[]{0x1D, 0x21, 0x10};

    public static final byte[] NORMAL_SIZE = new byte[]{0x1D, 0x21, 0x00};

    public static final byte[] UNICODE_TEXT = new byte[]{0x00, 0x50, 0x00,
            0x72, 0x00, 0x69, 0x00, 0x6E, 0x00, 0x74, 0x00, 0x20, 0x00, 0x20,
            0x00, 0x20, 0x00, 0x4D, 0x00, 0x65, 0x00, 0x73, 0x00, 0x73, 0x00,
            0x61, 0x00, 0x67, 0x00, 0x65};


    private SeekBar Brightness;
    private ImageView ImageVw;

    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {


        PluginResult pluginResult = null;
        context = this.cordova.getActivity().getApplicationContext();

        Log.i(TAG, "EXECUTE DEL BTPRINTPLUGIN");

        Log.i(TAG, "REGISTERING RECEIVERS");
        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        context.registerReceiver(mReceiver, filter);


        // Register for broadcasts when discovery starts
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        context.registerReceiver(mReceiver, filter);


        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        context.registerReceiver(mReceiver, filter);


        // Register for broadcasts when connectivity state changes
        filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(mReceiver, filter);

        Log.i(TAG, "Looper.prepare()");
        //Looper.prepare();
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }

        Log.i(TAG, "Geting default BT adapter - searchign for devices");
        mBTAdapter = BluetoothAdapter.getDefaultAdapter();
        found_devices = new ArrayList<BluetoothDevice>();
        try {
            String jsonArgs = args.getString(0);

            Gson gson = new Gson();

            //Map<String,String> map = new HashMap<>();
            //map = (Map<String,String>) gson.fromJson(jsonArgs, map.getClass());


            //PrintInftoDTO printInftoDTO = gson.fromJson(map.get("message"),PrintInftoDTO.class);
            PrintInftoDTO printInftoDTO = gson.fromJson(jsonArgs,PrintInftoDTO.class);
            String text = printInftoDTO.getPrintText();
            String printetMAC = printInftoDTO.getPrinterMac();

            try {
                Log.i(TAG, "BluetoothPlugin - We're in " + ACTION_ENABLE_BT);

                boolean enabled = false;

                Log.i("BluetoothPlugin", "Enabling Bluetooth...");

                if (mBTAdapter.isEnabled()) {
                    enabled = true;
                } else {
                    enabled = mBTAdapter.enable();
                    SystemClock.sleep(3000);
                }

                Log.i(TAG, "BluetoothPlugin - " + ACTION_ENABLE_BT + " Returning " + "Result: " + enabled);
                pluginResult = new PluginResult(PluginResult.Status.OK, enabled);
            } catch (Exception Ex) {
                Log.i(TAG, "BluetoothPlugin - " + ACTION_ENABLE_BT + " -> Got Exception " + Ex.getMessage());
                pluginResult = new PluginResult(PluginResult.Status.ERROR);
            }

            System.out.println(action);

            CheckGC("onCreate_Start");
            pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Tag");
            wl.acquire();
            CheckGC("onCreate_End");


            CheckGC("Connect_Start");
            mBTDevice = mBTAdapter.getRemoteDevice(printetMAC);

            BluetoothSocket tmp = null;
            UUID dvcUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
            tmp = mBTDevice.createRfcommSocketToServiceRecord(dvcUUID);
            mBTSocket = tmp;
            mBTSocket.connect();
            OutputStream out = null;

            if (mBTSocket.getOutputStream() != null) {
                out = mBTSocket.getOutputStream();

                //Print TEXT

                setCenter();
                //writeNVLogo();
                writeEnterLine(2);
                //text = "1";
                byte[] bufferCommond = text.toString().trim().getBytes("GB2312");
                print(bufferCommond);
                writeEnterLine(2);
                setLeft();
                mBTSocket.close();
            }
            conn = false;
            pluginResult = new PluginResult(PluginResult.Status.OK);
        } catch (Exception Ex) {
            Log.d(TAG, "BluetoothPlugin - " + ACTION_ENABLE_BT + " Got Exception " + Ex.getMessage());
            pluginResult = new PluginResult(PluginResult.Status.ERROR);
        } finally {
            try {
                mBTSocket.close();
                conn = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        wl.release();
        CheckGC("onDestroy_End");
        callbackContext.sendPluginResult(pluginResult);
        return true;

    }

    void CheckGC(String FunctionName) {
        long VmfreeMemory = Runtime.getRuntime().freeMemory();
        long VmmaxMemory = Runtime.getRuntime().maxMemory();
        long VmtotalMemory = Runtime.getRuntime().totalMemory();
        long Memorypercentage = ((VmtotalMemory - VmfreeMemory) * 100) / VmtotalMemory;

        Log.i(TAG, FunctionName + "Before Memorypercentage" + Memorypercentage + "% VmtotalMemory["
                + VmtotalMemory + "] " + "VmfreeMemory[" + VmfreeMemory + "] " + "VmmaxMemory[" + VmmaxMemory
                + "] ");

        // Runtime.getRuntime().gc();
        System.runFinalization();
        System.gc();
        VmfreeMemory = Runtime.getRuntime().freeMemory();
        VmmaxMemory = Runtime.getRuntime().maxMemory();
        VmtotalMemory = Runtime.getRuntime().totalMemory();
        Memorypercentage = ((VmtotalMemory - VmfreeMemory) * 100) / VmtotalMemory;

        Log.i(TAG, FunctionName + "_After Memorypercentage" + Memorypercentage + "% VmtotalMemory["
                + VmtotalMemory + "] " + "VmfreeMemory[" + VmfreeMemory + "] " + "VmmaxMemory[" + VmmaxMemory
                + "] ");
    }

    public void PrintOutput(String FunctionName, byte[] Data) {
        for (int i = 0; i < Data.length; i++) {
            Log.i(TAG, FunctionName + "Data[" + i + "]=" + Data[i]);
        }
    }


    /**
     * BroadcastReceiver to receive bluetooth events
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            Log.i("BluetoothPlugin", "Action: " + action);

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Log.i("BluetoothPlugin", "Device found " + device.getName() + " " + device.getBondState() + " " + device.getAddress());

                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    Log.i("BluetoothPlugin", "Device not paired");
                    addDevice(device);
                } else Log.i("BluetoothPlugin", "Device already paired");

                // When discovery starts
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {

                Log.i("BluetoothPlugin", "Discovery started");
                setDiscovering(true);

                // When discovery finishes
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {

                Log.i("BluetoothPlugin", "Discovery finilized");
                setDiscovering(false);

            }
        }
    };

    public void setDiscovering(boolean state) {
        discovering = state;
    }


    public void addDevice(BluetoothDevice device) {
        if (!found_devices.contains(device)) {
            Log.i("BluetoothPlugin", "Device stored ");
            found_devices.add(device);
        }
    }


    @Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);
        CheckGC("onPause_End");
    }


    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
        CheckGC("onResume_End");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
//		if (mBxlService != null) {
//			mBxlService.Disconnect();
//			mBxlService = null;
//		}
//		wl.release();
//		if (DeviceMoldel == HTC_Desire) {
//			Runtime.getRuntime().exit(0);
//		}
//		CheckGC("onDestroy_End");
    }


    public static void printTest() {
        writeEnterLine(1);
        print(PRINTE_TEST);
        writeEnterLine(3);
    }


    public static void enterLine() {
        byte[] b = new byte[]{0x1B, 0x21, 0x00};
        print(b);
    }

    public static void setLineLegth() {
        byte[] b = new byte[]{0x1B, 0x33, 0x33};
        print(b);
    }


    public static void printTextCenterCmd() {
        print(ESC_ALIGN_LEFT);
        print(ESC_HORIZONTAL_CENTERS);
    }


    /**
     * print photo with path ...
     *
     * @param :photo/pic.bmp
     */
    public static void printPhotoWithPath(String filePath) {

        String SDPath = Environment.getExternalStorageDirectory() + "/";
        String path = SDPath + filePath;

        File mfile = new File(path);
        if (mfile.exists()) {
            Bitmap bmp = BitmapFactory.decodeFile(path);
            byte[] command = decodeBitmap(bmp);
            printPhoto(command);
        } else {
            Log.e("PrintTools_58mm", "the file isn't exists");
        }
    }

    /**
     * print photo in assets
     *
     * @param :pic.bmp
     */
    public static void printPhotoInAssets(Context context, String fileName) {

        AssetManager asm = context.getResources().getAssets();
        InputStream is;
        try {
            is = asm.open(fileName);
            Bitmap bmp = BitmapFactory.decodeStream(is);
            is.close();
            if (bmp != null) {
                byte[] command = decodeBitmap(bmp);
                printPhoto(command);
            } else {
                Log.e("PrintTools", "the file isn't exists");
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    /**
     * decode bitmap to bytes bitmap
     */
    public static byte[] decodeBitmap(Bitmap bmp) {
        int bmpWidth = bmp.getWidth();
        int bmpHeight = bmp.getHeight();

        List<String> list = new ArrayList<String>(); //binaryString list
        StringBuffer sb;

        int bitLen = bmpWidth / 8;
        int zeroCount = bmpWidth % 8;
        String zeroStr = "";
        if (zeroCount > 0) {
            bitLen = bmpWidth / 8 + 1;
            for (int i = 0; i < (8 - zeroCount); i++) {
                zeroStr = zeroStr + "0";
            }
        }
        for (int i = 0; i < bmpHeight; i++) {
            sb = new StringBuffer();
            for (int j = 0; j < bmpWidth; j++) {
                int color = bmp.getPixel(j, i);
                //R G B
                int r = (color >> 16) & 0xff;
                int g = (color >> 8) & 0xff;
                int b = color & 0xff;

                // if color close to whitbit='0', else bit='1'
                if (r > 160 && g > 160 && b > 160)
                    sb.append("0");
                else
                    sb.append("1");
            }
            if (zeroCount > 0) {
                sb.append(zeroStr);
            }
            list.add(sb.toString());
        }
        // binaryStr
        List<String> bmpHexList = ConvertUtil.binaryListToHexStringList(list);
        String commandHexString = "1D763000";
        String widthHexString = Integer
                .toHexString(bmpWidth % 8 == 0 ? bmpWidth / 8
                        : (bmpWidth / 8 + 1));
        if (widthHexString.length() > 2) {
            Log.e("decodeBitmap error", "...width is too large");
            return null;
        } else if (widthHexString.length() == 1) {
            widthHexString = "0" + widthHexString;
        }
        widthHexString = widthHexString + "00";

        String heightHexString = Integer.toHexString(bmpHeight);
        if (heightHexString.length() > 2) {
            Log.e("decodeBitmap error", "... height is too large");
            return null;
        } else if (heightHexString.length() == 1) {
            heightHexString = "0" + heightHexString;
        }
        heightHexString = heightHexString + "00";

        List<String> commandList = new ArrayList<String>();
        commandList.add(commandHexString + widthHexString + heightHexString);
        commandList.addAll(bmpHexList);

        return ConvertUtil.hexList2Byte(commandList);
    }

    /**
     * print photo with bytes ...
     */
    public static void printPhoto(byte[] bytes) {
        print(ESC_ALIGN_CENTER);
        writeEnterLine(1);
        print(bytes);
        writeEnterLine(3);
    }


    /**
     * @param
     */
    public static void print(byte[] b) {

        try {
            OutputStream out = mBTSocket.getOutputStream();
            flush(out, b);
            //out.write(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * EnterLine
     *
     * @param
     */
    public static void writeEnterLine(int count) {
        for (int i = 0; i < count; i++) {
            print(ESC_ENTER);

        }

    }

    public static String getEnterLine(int count) {
        StringBuilder sBuilder = new StringBuilder();
        sBuilder.append(ESC_ENTER);
        return sBuilder.toString();
    }


    public static void writeNVLogo() {
        print(NV_LOGO);
    }

    public static void setH1Size() {
        print(H1_SIZE);
    }

    public static void setNormalSize() {
        print(NORMAL_SIZE);
    }

    public static void setCenter() {
        print(CENTER_POSITION);
    }

    public static void setLeft() {
        print(LEFT_POSITION);
    }


    public static void flush(OutputStream mmOutputStream, byte[] out) throws IOException {
        InputStream is = new ByteArrayInputStream(out);
        final int buffer_size = 512;
        byte[] bytes = new byte[buffer_size];
        for (int count = 0; count != -1; ) {
            count = is.read(bytes);
            if (count != -1) {
                mmOutputStream.write(bytes, 0, count);
                mmOutputStream.flush();
            }
        }
        mmOutputStream.flush();
        is.close();

        //out.reset();
    }


//    private void SetDitherMode(HPManPrintImage.dither dither) {
//        DitherMode=dither;
//        //Brightness.setProgress(128);
//        PrintImage.PrepareImage(DitherMode, 128);
//        //ImageVw.setImageBitmap(PrintImage.getPrintImage());
//    }

    public void SendDataRaw(byte buffer[]) throws Exception {
        OutputStream out = mBTSocket.getOutputStream();
        try {
            out.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}