// Empty constructor
function BtPrintPlugin() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
BtPrintPlugin.prototype.print = function(message, successCallback, errorCallback) {
  //options.duration = duration;
  cordova.exec(successCallback, errorCallback, 'BtPrintPlugin', 'print', [message]);
}

// Installation constructor that binds MiPlugin to window
BtPrintPlugin.install = function() {
    console.log("INSIDE BTPRINTPLUGIN - INSTALL FUNCTION")
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.btPrintPlugin = new BtPrintPlugin();
  return window.plugins.btPrintPlugin;
};
cordova.addConstructor(BtPrintPlugin.install);
